class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else {
			this.grades = grades;
		};
	};

	login(){

        console.log(`${this.email} has logged in`);
        return this;
    };

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    };

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        });
        return this;
    };

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    };

    willPass() {
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    };

    willPassWithHonors() {

       this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
       return this;
    };
};

// class Section will allow us to group our students as a section.

class Section {

    // every instance of Section class will be instantiated with an ampty array for our students.
    constructor(name){

        this.name = name;
        this.students = [];
        this.honorStudents = undefined;

    };
    // addStudents method will allow us to add instances of the Student class as items for our students property.
    addStudents(name,email,grades){

        // A Student instance/object will be instantiated with the name, email, and grades and pushed into our students property.

        this.students.push(new Student(name,email,grades));

        return this;

    };

    // countHonorStudents will loop over each Student instance in our students array and count the number of students who will pass with honors
    countHonorStudents(){
        // accumulate the number of honor students
        let count = 0;
        // loop over each Student instance in the students array
        this.students.forEach(student => {

            // log the student instance currently being iterated.
            // console.log(student);

            // log each student's isPassedWithHonors property:
            // console.log(student.willPassWithHonors().isPassedWithHonors);

            // invoke will pass with honors so we can determine and add whether the student passed with h onors in its property
            student.willPassWithHonors();

            // isPassedWithHonors property of the student should be populated
            // console.log(student);

            // Check if student isPassedWithHonor
            // add 1 to a temporary variable to hold the number of honorStudents
            
            if(student.isPassedWithHonors){
                count++;

            }

        });

        // update the honorStudents property with the updated value of count:
        this.honorStudents = count;
        return this;

    };

    getNumberOfStudents(){

        let count = 0;

        this.students.forEach(student => {

            count++;

        });

        return count;

    };

    countPassedStudents(){

        let count = 0;

        this.students.forEach(student => {

            student.willPass();
            if(student.isPassed){
                count++;
            };

        });

        this.passedStudents = count;
        return this;

    };

    computeSectionAve(){

        let totalAve = 0;

        this.students.forEach(student => {

            student.computeAve();
            totalAve += student.average

        });

        this.sectionAve = totalAve / this.students.length;
        return this;

    };

};

let section1A = new Section("Section1A");
console.log(section1A);

section1A.addStudents("Joy","joy@mail.com",[89,91,92,88]);
section1A.addStudents("Jeff","jeff@mail.com",[81,80,82,78]);
section1A.addStudents("John","john@mail.com",[91,90,92,96]);
section1A.addStudents("Jack","jack@mail.com",[95,92,92,93]);
section1A.addStudents("Alex","alex@mail.com",[84,85,85,86]);

console.log(section1A);

// // check the details of John from section1A?

// console.log(section1A.students[2]);

// // check if John from section1A?

// section1A.students[2].computeAve()

// // check the average of John from Section1A?
// console.log("John's average:",section1A.students[2].average);

// // check if Jeff from section1A passed?

// section1A.students[1].computeAve().willPass();

// console.log("Did Jeff pass?",section1A.students[1].isPassed);

// section1A.countHonorStudents();

// check the number of students:
console.log(section1A.countHonorStudents().honorStudents);

console.log(section1A.getNumberOfStudents());

console.log(section1A.countPassedStudents());

console.log(section1A.computeSectionAve());


